@rem ----[ This code block detects if the script is being running with admin PRIVILEGES If it isn't it pauses and then quits]-------
echo OFF
NET SESSION >nul 2>&1
IF %ERRORLEVEL% EQU 0 (
    ECHO Administrator PRIVILEGES Detected!
) ELSE (
   echo ######## ########  ########   #######  ########
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ######   ########  ########  ##     ## ########
   echo ##       ##   ##   ##   ##   ##     ## ##   ##
   echo ##       ##    ##  ##    ##  ##     ## ##    ##
   echo ######## ##     ## ##     ##  #######  ##     ##
   echo.
   echo.
   echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
   echo This script must be run as administrator to work properly!
   echo right click on the bat and select "Run As Administrator".
   echo ##########################################################
   echo.
   PAUSE
   EXIT /B 1
)
@echo OFF
echo "Installing NFS"
vagrant plugin install vagrant-winnfsd
echo "Setting nfs as windows8 compatibility mode"
reg.exe Add "HKLM\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers" /v "C:\Users\%userprofile%\.vagrant.d\gems\gems\vagrant-winnfsd-1.1.0\bin\winnfsd.exe" /d "~ WIN8RTM"
